[![ci-badge][]][ci] [![license-badge][]][license] [![docs-badge][]][docs]

# hummingbird.rs

An unofficial Rust wrapper for the [hummingbird.me] API.

### Installation

Add the following dependency to your Cargo.toml:

```rust
hummingbird = "0.1"
```

And include it in your project:

```rust
extern crate hummingbird;
```

### License

License info in [LICENSE.md]. Long story short, ISC.

[ci-badge]: https://gitlab.com/kalasi/hummingbird.rs/badges/master/build.svg
[ci]: https://gitlab.com/kalasi/hummingbird.rs/pipelines
[hummingbird.me]: https://hummingbird.me
[license-badge]: https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square
[license]: https://opensource.org/licenses/ISC
[docs-badge]: https://img.shields.io/badge/docs-online-2020ff.svg
[docs]: https://docs.austinhellyer.me/hummingbird/
[examples]: https://gitlab.com/kalasi/hummingbird.rs/tree/master/examples
[LICENSE.md]: https://gitlab.com/kalasi/hummingbird.rs/blob/master/LICENSE.md
