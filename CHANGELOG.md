# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning][semver].

## [Unreleased]

### Added

### Changed

## [0.1.3] 2016-11-02

### Added

- Register AgeRating::TvY7

## [0.1.2] 2016-08-30

### Added

### Changed

- `Anime::mal_id` is now an optional field

## [0.1.1] - 2016-08-30

### Added

- Catch the AgeRating "PG-13" from the API

### Changed

## [0.1.0] - 2016-08-22

Initial commit.

[Unreleased]: https://gitlab.com/kalasi/hummingbird.rs/compare/v0.1.3...master
[0.1.3]: https://gitlab.com/kalasi/hummingbird.rs/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/kalasi/hummingbird.rs/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/kalasi/hummingbird.rs/compare/v0.1.0...v0.1.1
[semver]: http://semver.org
